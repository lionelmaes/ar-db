# ar-db

Building a new website for http://art-recherche.be, using Collective Access.

## Documents types
* bibo:Article
* bibo:AudioVisualDocument
* bibo:Book
* bibo:BookSection
* bibo:Code
* bibo:Conference
* bibo:Event
* bibo:Image
* bibo:Interview
* bibo:Letter
* bibo:Note
* bibo:Periodical
* bibo:Quote
* bibo:Report
* bibo:Series
* bibo:Thesis
* bibo:Webpage

## Relations types
* cito: agrees with
* cito: cites
* cito: compiles
* cito: critiques
* cito: describes
* cito: disagrees with
* cito: discusses
* cito: extends
* cito: gives background to
* cito: includes excerpt from (provides excerpt for)
* cito: replies to
* cito: reviews
* cito: shares author with
* cito: speculates on
* cito: updates
* cito: uses data from
