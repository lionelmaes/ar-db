<?php

$preloads = array(
    'BrowseService.php',
    'ItemService.php',
    'ModelService.php',
    'SearchService.php'
);

foreach($preloads as $file) {

	include_once($file);
}

unset($preloads);
?>
