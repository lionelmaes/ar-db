<?php namespace ProcessWire;

/**
 * TRANSLATABLE STRINGS
 * Define globally available translatable strings
 *
 * USAGE
 * place this file under /site/translations.php
 * The wire property $ferry refers to the textdomain of this file
 *
 */
__('search');
__('locale');
__('type your research');
__('title');
__('creation date');
__('in progress');
