
<?php if($page->editable()) echo "<p><a href='$page->editURL'>Edit</a></p>"; ?>
</main>
<script src="<?php echo $config->urls->templates?>scripts/main.js"></script>
<?php
if($page->name == 'database'):
 ?>
 <script src="<?php echo $config->urls->templates?>scripts/autoComplete.js"></script>
 <script src="<?php echo $config->urls->templates?>scripts/filters.js"></script>

<?php
endif;
?>
</body>
</html>
