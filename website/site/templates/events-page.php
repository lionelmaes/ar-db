<?php
namespace ProcessWire;
if(!$config->ajax){
    include('header.inc');
}

include('news-loader.php');

if($pagesBefore):

?>
<div class="loading" data-direction="back" data-section="<?php echo $childrenPages[0]->id; ?>"></div>
<?php
endif;
foreach($childrenPages as $childPage):
    $images = $childPage->images;

    //check if event is happening now
    $startT = $datetime->date('ts', $childPage->start_date);
    
    $endT = $datetime->date('ts', $childPage->end_date);
    
    $nowT = $datetime->date('ts');
    

    
    if($nowT >= $startT && $nowT <= $endT){
        $timePos = 'now';
        $timePosStr = __('in progress', $tr);
    }
    else if($nowT > $endT){
        $timePos = 'past';
        $timePosStr = $datetime->relativeTimeStr($childPage->end_date);
        
    }
    else{
        $timePos = 'future';
        $timePosStr = $datetime->relativeTimeStr($childPage->start_date);
    }

?>
<section id="<?php echo 'evt-'.$childPage->name; ?>" class="time-<?php echo $timePos.(($visible)?' visible':''); ?>">
    <h1>
       
        <span class="time-pos"><?php echo $timePosStr; ?></span>
        <span class="title">
        <?php echo $childPage->title; ?>
        </span>
        <?php if ($childPage->place): ?>
              <span class="place"><?php echo $childPage->place; ?></span>
        <?php endif; ?>
        <?php if ($images->first()): ?>
            <!--img src="<?php echo $images->first->url ?>" alt=""-->
        <?php endif; ?>
        

    </h1>
    
    <div class="practical">
          
          <?php 
          if($datetime->date('Y-m-d', $startT) == $datetime->date('Y-m-d', $endT)){
              switch($user->language->name){
                  case 'fr':
                    $languages->setLocale(LC_ALL, 'fr_FR.UTF-8');
                    echo $datetime->date('%A %e %B %Y', $startT).' <br>de '
                    .$datetime->date('H:i', $startT).' à '.$datetime->date('H:i', $endT);
                    break;
                  default:
                    echo $datetime->date('l, j F Y', $startT).' <br>from '
                    .$datetime->date('%r', $startT).' to '.$datetime->date('%r', $endT);
                  
              }
          }else{
            switch($user->language->name){
                case 'fr':
                  $languages->setLocale(LC_ALL, 'fr_FR.UTF-8');
                  echo 'du '.$datetime->date('%A %e %B %Y', $startT).' <br>à '
                  .$datetime->date('H:i', $startT).' <br>au '.$datetime->date('%A %e %B %Y', $endT).'<br> à '
                  .$datetime->date('H:i', $endT);
                  break;
                default:
                  echo 'from '.$datetime->date('l, j F Y', $startT).' <br>at '
                  .$datetime->date('%r', $startT).' <br>to '.$datetime->date('l, j F Y', $startT).'<br> at '
                  .$datetime->date('%r', $endT);
                
            }
          }
          echo $childPage->practicals; 
          
          ?>
<?php
            if ($images):
            foreach ($images as $image):
?>
              <img src="<?php echo $image->url ?>" alt="">
<?php
            endforeach;
            endif;
            
          ?>
    </div>

    <div class="text"><?php echo $childPage->text ?></div>


</section>
<?php

endforeach;
if($pagesAfter):
?>
<div class="loading" data-direction="fwd" data-section="<?php echo $childrenPages[count($childrenPages) - 1]->id; ?>"></div>
<?php
endif;
if(!$config->ajax)
    include('footer.inc');

?>