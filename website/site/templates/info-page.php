<?php
  include('header.inc'); 
?>
<?php if($page->children->count() > 0):
?>
 <ul class="sequence-menu h-scroll sub-scroll">
  <?php
  $activeString = ' class="active"';
  foreach($page->children as $childPage):
  ?>
      <li<?php echo $activeString; ?>>
        <a href="#<?php echo $childPage->name; ?>">
          <?php echo $childPage->title; ?>
        </a>
    </li>
  <?php
  $activeString = '';
  endforeach;
  ?>
</ul>
<?php endif; ?>

<?php if($page->text != ''): ?>
<section class="main-content">
<?= $page->text; ?> 
</section>
<?php endif; ?>

<?php
foreach($page->children as $childPage):
?>

<section id="<?php echo $childPage->name; ?>">
<div class="text"><?= $childPage->text ?></div>
<?php
  if($childPage->images):
?>
    <div class="gallery">
<?php
    foreach($childPage->images as $image):
?>
  <img src="<?= $image->width(1000)->url ?>">
<?php
    endforeach;
?>
  </div>
<?php
  endif;
?>
</section>
 

<?php
endforeach;
?>
<?php
  include('footer.inc');
?>