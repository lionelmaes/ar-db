<?php
  include_once('db/CAController.php');

  $ca = new CAController($config->CAURL, $config->CAUser, $config->CAKey, __('locale', $tr), $page);

  if(!$config->ajax){

    include('header.inc');

    //echo 'noajax';
    $facets = array("access_facet"=> array(1));
  }
  else{
    $parameters = json_decode(file_get_contents("php://input"), true);
    $facets = $ca->buildFacetsRequest($parameters['facets']);
    //print_r($facets);
    //print_r(json_decode(file_get_contents("php://input"), true));

  }

  if($input->urlSegment1 == ''){//listing page
    if(!$config->ajax || isset($parameters['action']) && $parameters['action'] == 'reload'){
      include('db/db_listing_filters.php');
      include('db/db_listing.php');
    }else{
      include('db/db_listing.php');
    }
  }
  else{

    preg_match('/[A-Z]+\./', $input->urlSegment1, $matches);
    $thingType = (count($matches) == 0)?false:$matches[0];

    switch($thingType){
      case 'ENT.':
        $entId = $input->urlSegment1;
        include('db/db_ent_detail.php');
        break;
      case 'DOC.':
        $docId = $input->urlSegment1;
        $single = true;
        include('db/db_doc_detail.php');
        break;
      case 'COL.':
        include('db/db_proj_detail.php');
        break;
      default:
        include('db/db_listing_filters.php');
        include('db/db_listing.php');
        break;
    }


  }
?>




<?php
  if(!$config->ajax):
?>

<script>
  const searchDefaultMessage = "<?php echo __('search', $tr); ?>...";
</script>

<?php
  include('footer.inc');
  endif;
?>
