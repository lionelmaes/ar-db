<?php
$childrenPages = array();
$nbPagesByRequest = 3;
$pagesBefore = false;
$pagesAfter = false;
$visible = false;


if(!$config->ajax){
    //either 1 page from urlsegment
    if($input->urlSegment1 != ''){
        $targetPage = $pages->findOne('name='.str_replace(array('evt-', 'news-'), '', $input->urlSegment1));
        if($targetPage->id != 0){
            $childrenPages[] = $targetPage;
            $visible = true;
        }
        if($targetPage->next()->id != 0){
            $pagesAfter = true;
        }
        if($targetPage->prev()->id != 0){
            $pagesBefore = true;
        }
    }
    //or load x first pages if no urlsegment
    if($input->urlSegment1 == '' || count($childrenPages) == 0){
        $childrenPages = $page->children('limit='.$nbPagesByRequest.',sort=-publication_date|-end_date');
        if($childrenPages[ count($childrenPages) -1 ]->next()->id != 0){
            $pagesAfter = true;
        }
    }
}else{

    $parameters = json_decode(file_get_contents("php://input"), true);
    //echo 'start='.$parameters['section'].',limit='.$nbPagesByRequest;
    if($parameters['direction'] == 'fwd'){
        $childrenPages = $page->children('sort=-publication_date|-end_date, limit='.$nbPagesByRequest, array('startAfterID'=>$parameters['section']));

        if($childrenPages[ count($childrenPages) -1 ]->next()->id != 0){
            $pagesAfter = true;
        }
    }else{
        $childrenPages = $page->children('sort=-publication_date|-end_date, limit='.$nbPagesByRequest, array('stopBeforeID'=>$parameters['section']));
        //echo $childrenPages[0]->prev()->id;
        if($childrenPages[0]->prev()->id != 0){
            $pagesBefore = true;
        }

    }

    //echo $parameters['direction'];
    //echo $parameters['section'];
}
?>