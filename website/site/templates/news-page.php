<?php

namespace ProcessWire;
if(!$config->ajax){
    include('header.inc');
}

include('news-loader.php');

if($pagesBefore):

?>
<div class="loading" data-direction="back" data-section="<?php echo $childrenPages[0]->id; ?>"></div>
<?php
endif;
foreach($childrenPages as $childPage):
    $images = $childPage->images;
?>
<section id="<?php echo 'news-'.$childPage->name; ?>" class="<?php echo ($visible)?'visible':''; ?>">
    <h1>
        <span class="title">
        <?php echo $childPage->title; ?>
        </span>
        <?php if ($images->first()): ?>
            <!--img src="<?php echo $images->first->url ?>" alt=""-->
        <?php endif; ?>
        <span class="date"><?php echo $childPage->publication_date; ?></span>

    </h1>
    
    <div class="practical">
          <?php echo $childPage->practicals; ?>
<?php
            if ($images):
            foreach ($images as $image):
?>
              <img src="<?php echo $image->url ?>" alt="">
<?php
            endforeach;
            endif;
            
          ?>
    </div>

    <div class="text"><?php echo $childPage->text ?></div>


</section>
<?php

endforeach;
if($pagesAfter):
?>
<div class="loading" data-direction="fwd" data-section="<?php echo $childrenPages[count($childrenPages) - 1]->id; ?>"></div>
<?php
endif;
if(!$config->ajax)
    include('footer.inc');

?>