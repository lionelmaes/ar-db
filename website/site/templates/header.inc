<?php

$urlSegments = $input->urlSegment1.(($input->urlSegment2 != '')?'/'.$input->urlSegment2:'');
?>
<!DOCTYPE html>
<html lang="<?php echo $user->language; ?>">
	<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php echo $page->title; ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/transitions.css" />

<?php
if($page->name == 'database'):
 ?>
 		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/autoComplete.css" />
		<script src="https://unpkg.com/colcade@0/colcade.js"></script>
<?php
endif;
?>


  </head>
	<body>

    <nav>
      <div class="mobile ui">
        <div class="burger slider-button" data-target="#main-nav">
        </div>
        <div class="lng">
<?php  
$savedLanguage = $user->language;
$i = 0;

foreach($languages as $language) {

    $user->language = $language;

    echo '<a href="'.$page->url.$urlSegments.'"'.(($savedLanguage == $user->language)?' class="active"':'').'>'
    .substr($language->title, 0, 2).'</a>'.(($i != count($languages) - 1)?' | ':'');
    $i++;
}
// restore the original language setting
$user->language = $savedLanguage;
$root = $pages->get("/");
?>
      </div>
    </div>
    <div id="main-nav" class="slider when-mobile">
      <ul>
      <?php
      $menuPages = $pages->find('parent=/');
      foreach($menuPages as $menuPage){
          echo '<li'.(($page == $menuPage || $page->parent == $menuPage)?'
          class="active"':'').'><a href="'.$menuPage->url.'">'.
          $menuPage->title.
          '</a></li>';
      }
      ?>
      <li class="lng desktop">
<?php
$savedLanguage = $user->language;
$i = 0;

foreach($languages as $language) {

    $user->language = $language;

    echo '<a href="'.$page->url.$urlSegments.'"'.(($savedLanguage == $user->language)?' class="active"':'').'>'
    .substr($language->title, 0, 2).'</a>'.(($i != count($languages) - 1)?' | ':'');
    $i++;
}
// restore the original language setting
$user->language = $savedLanguage;
?>
      </li>
      </ul>
    </div>
    </nav>
    <main id="<?php echo $page->name; ?>" class="<?php echo $page->name.' '.$page->template.(($page->css_classes)?' '.$page->css_classes:''); ?>" data-base-url="<?php echo $page->url; ?>">
