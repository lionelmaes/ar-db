<?php

  include('header.inc');
  $childrens = $page->children;

?>

<?php 
//SUBMENU FOR INFO AND ABOUT PAGES
if($page->name == 'info' || $page->name == 'about'):
?>
  <ul class="sequence-menu h-scroll">
  <?php
  foreach($page->children as $childPage):
  ?>
      <li class="active">
        <a href="#">
          <?php echo $childPage->title; ?>
        </a>
    </li>
  <?php
  endforeach;
  ?>
    </ul>

<?php
  endif;
?>

<?php 
  

    foreach($childrens as $children){
      $date = date("m/j/y", $children->created);
      $images = $children->images;
      ?><section id="<?= $children->id ?>">

        <h1>
          <span class="title">
            <?= $children->title ?>
            <?php if ($children->place) { ?>
              <span class="place"><?= $children->place ?></span>
            <?php } ?>
          </span>
            <?php if ($children->template == 'news') { ?>
              <?php if ($images->first()) { ?>
                <img src="<?= $images->first->url ?>" alt="">
              <?php } ?>
              <span class="date"><?= $date ?></span>
            <?php } ?>
        </h1>

        <div class="practical">
          <?= $children->practicals?>
          <?php
            if ($images) {
            foreach ($images as $image) {
              ?>
              <img src="<?= $image->url ?>" alt="">
              <?php
              }
            }
          ?>
        </div>

        <div class="text"><?= $children->text ?></div>

        <?php
          if ($children->children) {
            foreach ($children->children as $subChildren) {
              ?>
              <section class="subSection" id="<?= $subChildren->id ?>">
                <h4><?= $subChildren->title ?></h4>
                <div class="text"><?= $subChildren->text ?></div>
              </section>

              <?php
            }
          }
        ?>
        <div class="clear"></div>
      </section><?php
    }

  include('footer.inc');
?>
