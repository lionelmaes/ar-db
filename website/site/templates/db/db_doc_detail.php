<?php

  $detail = $ca->getObjectDetail($docId);
  //print_r($detail);
  //print_r($detail);
  //echo $input->urlSegment1;

?>
<section class="object-detail" <?php echo (isset($objCounter))?'id="obj-'.$objCounter.'"':'';?>>
  <header>
    <h3 class="h-scroll">
<?php
  $projectExists = array_key_exists('relations', $detail) && array_key_exists('projects', $detail['relations']);

  if(isset($single) && $single === true && $projectExists):
    foreach($detail['relations']['projects'] as $projIdno => $projData):
      if($projData['pole'] == false) continue;
?>
      <a class="project-path" href="<?php echo $page->url.$projIdno ?>">
        <span class="pole"><?php echo $projData['pole']; ?></span>
        <span class="project"><?php echo $projData['name']; ?></span></a>
<?php
    endforeach;
?>
 —
<?php
  endif;
?>
    <span class="object-title"><?php echo ((isset($objCounter))?$objCounter.'. ':'').$detail['title']; ?></span>
    </h3>
  </header>
  <section class="detail-container">
    <!-- RELATIONS -->
    <?php

    if((count($detail['relations']['sequences']) > 0 && !isset($currentSequence))
        || count($detail['relations']['sequences']) > 1):
    ?>
      <section class="relations relations-sequences">
        <ul class="h-scroll">
    <?php
    foreach($detail['relations']['sequences'] as $relIdno => $rel):

    ?>
        <li>
          <span class="relation-type"><?php echo $rel['relation']; ?></span>
          <a href="<?php echo $page->url.$rel['projects'][0]['idno'].'/'.$relIdno; ?>"><?php echo $rel['projects'][0]['name'].': '.$rel['name']; ?></a>
        </li>
    <?php
    endforeach;
    ?>
      </ul>
      </section>
    <?php
    endif;
    ?>

    <?php
    if(count($detail['relations']['ca_objects']) > 0):
    ?>
        <section class="relations relations-objects">
          <ul class="h-scroll">
    <?php
    foreach($detail['relations']['ca_objects'] as $relIdno => $rel):
    ?>
          <li>
            <span class="relation-type"><?php echo $rel['relation']; ?></span>
            <a href="<?php echo $page->url.$relIdno; ?>"><?php echo $rel['name']; ?></a>
          </li>
    <?php
    endforeach;
    ?>
          </ul>
        </section>
    <?php
    endif;
    ?>
    <?php
    if(count($detail['relations']['ca_entities']) > 0):
    ?>
        <section class="relations relations-entities">
          <ul class="h-scroll">
    <?php
    foreach($detail['relations']['ca_entities'] as $relIdno => $rel):
    ?>
          <li>
            <span class="relation-type"><?php echo $rel['relation']; ?></span>
            <a href="<?php echo $page->url.$relIdno; ?>"><?php echo $rel['name']; ?></a>
          </li>
    <?php
    endforeach;
    ?>
          </ul>
        </section>
    <?php
    endif;
    ?>



    <!-- SUMMARY -->
    <section class="document-summary">
      <ul>
  <?php
  foreach($detail['infos'] as $label => $value):
    if(is_array($value))
      $value = implode(', ', $value);
  ?>

        <li>
          <span class="label"><?php echo $label; ?></span>:
          <span class="value"><?php echo $value; ?></span>
        </li>
  <?php
  endforeach;
  ?>
      </ul>
    </section>
  <!-- MEDIAS -->
    <section class="document-medias h-scroll">
<?php
foreach($detail['medias'] as $media):

?>
      <section class="document-media">
        <div class="media-preview">
<?php
  if(preg_match('#/support/icons#', $media['thumbnail'])){
    $media['thumbnail'] = $config->urls->templates.'imgs/doc.png';
  }

  if($media['type'] == 'video/mp4' || $media['type'] == 'video/webm'):

?>
    <video poster="<?php echo $media['thumbnail']; ?>" controls>
      <source src="<?php echo $media['original']; ?>" type="<?php echo $media['type']; ?>">
    </video>

<?php
  else:
?>
        <img src="<?php echo $media['thumbnail']; ?>">
<?php
  endif;
?>
      </div>
      <div class="media-detail">
        <ul>
          <?php if($media['name'] != '[BLANK]'): ?>
          <li><?php echo $media['name']; ?></li>
          <?php endif; ?>
          <li><?php echo $media['original_filename']; ?></li>
          <li><?php echo $media['type']; ?></li>
          <li><?php echo formatBytes($media['filesize']); ?></li>
          <li><a href="<?php echo $media['original']; ?>"><?php echo "download"; ?></a></li>

        </ul>
      </div>



    </section>
<?php
endforeach;
?>
    </section>

<?php
foreach($detail['content'] as $content):
  if(is_array($content))
    $content = $content[0];
?>
    <section class="text-body">
      <p>
      <?php echo $content; ?>
      </p>
    </section>
<?php
endforeach;
?>

</section>
</section>
