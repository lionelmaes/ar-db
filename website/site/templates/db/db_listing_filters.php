
<section id="search-ui">
<ul>
  <li id="search-field">
<?php
  if(isset($parameters)):
?>
  <div class="facet-params">
<?php
    foreach($parameters['facets'] as $facetKey => $facetsDetail):
      foreach($facetsDetail as $facetDetail):
?>
    <span class="facet-param"
      data-facet-value="<?php echo $facetDetail['value']; ?>"
      data-facet-key="<?php echo $facetKey; ?>">
      <?php echo $facetDetail['label'].': '.$facetDetail['text']; ?>
    </span>
<?php
      endforeach;
    endforeach;
?>
  </div><?php
  endif;
?><span class="item-title"><input id="autoComplete" tabindex="1"></span>
  </li>
  <li id="search-filters">
    <ul>
<?php
  $availFacets = $ca->getFacets($facets);
  //print_r($availFacets);
  //print_r($availFacets);
  foreach($availFacets as $keyFacet => $facet):
?>
  <li><span class="item-title slider-button" data-target="#facets-<?php echo $keyFacet; ?>"><?php echo $facet['label_plural'];?></span>
    <ul class="slider" id="facets-<?php echo $keyFacet; ?>">
<?php
    if($facet['group_mode'] != 'none'):
      $groupLabels = array();
      $counter = 0;
      foreach($facet['content'] as $groupLabel => $fcs):
        $groupLabels[]  = $groupLabel;

?>
      <!--li class="facet-group group-<?php echo $groupLabel; ?>">

        <ul-->
<?php

          foreach($fcs as $fc):
            if($counter % 7 == 0):
            //print_r($fc);
?>
        <li class="facet-group">
          <ul>
<?php
            endif;
?>
            <li><span class="item-title facet"
              data-facet-key="<?php echo $keyFacet; ?>"
              data-facet-label="<?php echo $facet['label_singular']; ?>"
              data-facet-value="<?php echo $fc['id']; ?>">
              <?php echo $fc['label']; //echo '('.$fc['content_count'].')'; ?></span>
            </li>
         

<?php
          $counter++;
          
          if($counter % 7 == 0):
?>
          </ul>
        </li>
<?php
          endif;
          
        endforeach;
?>
        <!--/ul>
      </li-->
<?php
      endforeach;
    else:
      $counter = 0;
      foreach($facet['content'] as $fc):
        if($counter % 7 == 0):
          //print_r($fc);
?>

  <li class="facet-group">
    <ul>
<?php
        endif;
?>
      <li>
        <span class="item-title facet"
          data-facet-key="<?php echo $keyFacet; ?>"
          data-facet-label="<?php echo $facet['label_singular']; ?>"
          data-facet-value="<?php echo $fc['id']; ?>">
          <?php echo $fc['label']; //echo '('.$fc['content_count'].')'; ?></span>
      </li>
<?php
        $counter++;
        if($counter % 7 == 0):
?>
    </ul>
  </li>
<?php
        endif;
?>
<?php
      
      endforeach;
    endif;
    if($counter % 7 != 0)
        echo '</ul></li>';
?>
    </ul>
  </li>
<?php
  endforeach;
?>
    </ul>
  </li>
  <!--li id="search-validates"><span class="item-title"><?php echo __("search", $tr); ?></span></li-->
</ul>
</section>
