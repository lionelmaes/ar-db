<?php
  $detail = $ca->getEntityDetail($entId);
  //print_r($detail);
?>
<section class="entity-detail">
  <header>
    <h3>
      <?php echo $detail['name']; ?>
    </h3>
  </header>
  <section class="detail-container">
    <!-- RELATIONS -->
    <?php
    if(count($detail['objects']) > 0):
    ?>
    <section class="relations relations-objects">
      <ul>
      <?php
        foreach($detail['objects'] as $relIdno => $rel):
      ?>
        <li>
          <span class="relation-type"><?php echo $rel['relation']; ?></span>
          <a href="<?php echo $page->url.$relIdno; ?>"><?php echo $rel['name']; ?></a>
        </li>
      <?php
        endforeach;
      ?>
      </ul>
    </section>
  <?php endif; ?>
  <?php
  if(isset($detail['biography'])):
  ?>
      <section class="text-body">
        <p>
        <?php echo $detail['biography']; ?>
        </p>
      </section>
  <?php
  endif;
  ?>
 </section>
 </section>
