<?php

//send request and get data from CA instance
class CAController{
  private $CAurl = null;
  private $CAuser = null;
  private $CAkey = null;
  private $collectionModel = null;
  private $objectModel = null;
  private $entityModel = null;

  public function __construct($url, $user, $key, $locale, $page) {
    $this->CAurl = $url;
    $this->CAuser = $user;
    $this->CAkey = $key;
    $this->locale = $locale;
    //$this->objectModel = $this->getObjectModel();

    $this->page = $page;
    setlocale(LC_TIME, $this->locale);

  }

  public function buildFacetsRequest($facets){
    $request = array("access_facet"=> array(1));
    foreach($facets as $facetKey=>$facetArray){
      $request[$facetKey] = array();
      foreach($facetArray as $facet){
        $request[$facetKey][] = $facet['value'];
      }
    }
    return $request;
  } 

  public function getObjectModel(){
    return $this->getModel('ca_objects');
  }
  public function getCollectionModel(){
    return $this->getModel('ca_collections');
  }
  public function getEntityModel(){
    return $this->getModel('ca_entities');
  }
  private function getModel($table){
    $modelClient = new ModelService($this->CAurl, $this->CAuser, $this->CAkey, $table,"OPTIONS");
    $modelClient->addGetParameter('lang', $this->locale);
    $result = $modelClient->request();

    return $result->getRawData();
  }

  public function getFacets($facets){

    $browseClient = new BrowseService($this->CAurl, $this->CAuser, $this->CAkey, "ca_objects","OPTIONS");
    $browseClient->addGetParameter('lang', $this->locale);
    //$browseClient->addGetParameter('q'.rand(0,10), 1);
    $browseClient->setRequestBody(
      array("criteria" => $facets)
    );

    $result = $browseClient->request();
    //print_r($result->getRawData());
    for($i = 0; $i < 3; $i++){
      if(count($result->getRawData()) > 0)
        break;
      //echo "retry";
      sleep(1);
      $result = $browseClient->request();
    }
    return $result->getRawData();
  }

  public function getListing($facets, $start, $limit){
    $browseClient = new BrowseService($this->CAurl, $this->CAuser, $this->CAkey, "ca_objects","GET");
    $browseClient->addGetParameter('lang', $this->locale);
    $browseClient->addGetParameter('start', $start);
    $browseClient->addGetParameter('limit', $limit);
    $browseClient->addGetParameter('sort', 'ca_objects.idno');
    $browseClient->addGetParameter('sortDirection', 'desc');
    $browseClient->setRequestBody(
      array("criteria" => $facets)
    );
    //print_r( array("criteria" => $facets));
    $result = $browseClient->request();
    return $result->getRawData();
  }
  public function getEntityDetail($id){
    $itemClient = $this->getItemClient('ca_entities', $id);
    $itemClient->setRequestBody(
      array("bundles" => array(

        "ca_entities.preferred_labels.displayname" => array(

        ),
        'ca_entities.biography' => array(),
        'ca_objects_x_entities.type_id' => array(

        ),
        'ca_objects.related.preferred_labels.name' => array(
        ),
        'ca_objects.related.idno' => array(

        )
      )
     )
    );

    $result = $itemClient->request();
    $rawData = $result->getRawData();
    //print_r($rawData);
    $data = array(
      'name' => $rawData['ca_entities.preferred_labels.displayname'][0]['displayname'],

    );
    if(count($rawData['ca_entities.biography'][0]) > 0)
      $data['biography'] = $rawData['ca_entities.biography'][0]['biography'];

    $data['objects'] = array();
    $i = 0;
    foreach($rawData['ca_objects.related.idno'] as $objectId){
      $data['objects'][$objectId] = array(
        'name' => $rawData['ca_objects.related.preferred_labels.name'][$i]['name'],
        'relation'=>$this->getRelationName($rawData['ca_objects_x_entities.type_id'][$i], 'ca_entities', 'ca_objects', 'individual', true),

        'ref'=> $objectId
      );
      $i++;
    }
    return $data;
  }
  public function getObjectDetail($id){
    /* TODO: specify request body instead of full request on object */
    $itemClient = $this->getItemClient('ca_objects', $id);
    $result = $itemClient->request();
    $rawData = $result->getRawData();
    //print_r($rawData);
    $data = array('infos' => array(), 'relations' => array(), 'medias' => array(), 'content' => array());

    $data['title'] = $this->switchLocale($rawData['preferred_labels'])[0]['name'];
    /* info */

    $data['infos']['type'] = $this->switchLocale($rawData['type_id']['display_text']);
    $data['infos']['ref'] = $rawData['idno']['value'];
    foreach($rawData['ca_objects.date'] as $dateArray){
      $dateArray = $this->switchLocale($dateArray);
      if(!array_key_exists('dc_dates_types', $dateArray))
        continue;
      $date = array(
        'type' => $dateArray['dc_dates_types'],
        'value' => $dateArray['dates_value']
      );
      $data['infos'][$dateArray['dc_dates_types']] = $dateArray['dates_value'];

    }
    $infoFields = array('duration', 'description', 'rights', 'lcsh_language', 'tags');
    foreach($infoFields as $objectField)
      $data['infos'] = $this->parseValues($data['infos'], $rawData, $objectField);

    $contentFields = array('text_body');
    foreach($contentFields as $objectField){
      
      $data['content'] = $this->parseValues($data['content'], $rawData, $objectField);

    }

    /* relations */
    $relationTables = array('ca_objects', 'ca_entities');
    foreach($relationTables as $relationTable)
      $data['relations'] = $this->parseRelations($data['relations'], $rawData, $relationTable);

    $collections = array();
    $collections = $this->parseRelations($collections, $rawData, 'ca_collections');

    if(array_key_exists('ca_collections', $collections)){
      $projects = array();
      $sequences = array();
      foreach($collections['ca_collections'] as $idno => $values){
        if(substr($idno, 0, 8) == 'COL.SEQ.'){
          $proj = $this->getRelColFromColId($idno);

          $values['projects'][] = array('idno' => $proj['ca_collections.related.idno'][0],
            'name' => $proj['ca_collections.related.preferred_labels.name'][0]['name']);
          $sequences[$idno] = $values;
        }else{
          $values['pole'] = $this->getPoleFromColId($idno);
          $projects[$idno] = $values;
        }

        //if(substr($idno, 0, 8))
      }
      $data['relations']['projects'] = $projects;
      $data['relations']['sequences'] = $sequences;
    }
    /*medias*/

    $data['medias'] = $this->getObjectRepresentations($id);
    $i = 0;


    return $data;

  }

  public function getObjectRepresentations($id){
    $itemClient = $this->getItemClient('ca_objects', $id);
    $itemClient->setRequestBody(
      array("bundles" => array(
        "ca_object_representations.media.original.mimetype"  => array(),
        "ca_object_representations.media.original.WIDTH"  => array(),
        "ca_object_representations.media.original.HEIGHT"  => array(),
        "ca_object_representations.media.original.original_filename"  => array(),
        "ca_object_representations.media.original.PROPERTIES"  => array(
          'returnAllLocales' => true//without this we get an array with no way to differenciate the properties and the different files.
        ),
        "ca_object_representations.media.original.url" => array(),
        "ca_object_representations.media.large.url"  => array(),
        "ca_object_representations.media.h264_hi.url"  => array(),
        "ca_object_representations.preferred_labels.name"  => array()
        )
      )
    );

    $result = $itemClient->request();
    $rawData = $result->getRawData();
    $objectRepresentations = array();

    //print_r($rawData);
    //echo count($rawData['ca_object_representations.media.large.url']);
    for($i = 0; $i < count($rawData['ca_object_representations.media.large.url']); $i++){
      $representation = array(
        'thumbnail'=> $rawData['ca_object_representations.media.large.url'][$i],
        'original'=>$rawData['ca_object_representations.media.original.url'][$i],
        'name'=>$rawData['ca_object_representations.preferred_labels.name'][$i]['name'],
        'type'=>$rawData['ca_object_representations.media.original.mimetype'][$i],
        'original_filename'=>$rawData['ca_object_representations.media.original.original_filename'][$i],


        'filesize'=>$rawData['ca_object_representations.media.original.PROPERTIES'][$i]['filesize']
      );

      if(isset($rawData['ca_object_representations.media.original.WIDTH'][$i]))
        $representation['width'] = $rawData['ca_object_representations.media.original.WIDTH'][$i];
      if(isset($rawData['ca_object_representations.media.original.HEIGHT'][$i]))
        $representation['height'] = $rawData['ca_object_representations.media.original.HEIGHT'][$i];


      //echo $rawData['ca_object_representations.media.original.url'][0];
      $representation['extension'] = pathinfo($rawData['ca_object_representations.media.original.url'][$i], PATHINFO_EXTENSION);
      if($representation['type'] == 'application/octet-stream' &&
      $representation['extension'] == 'webm'){
        $representation['type'] = 'video/webm';
      }
      $objectRepresentations[] = $representation;
    }

    //print_r($objectRepresentations);
    return $objectRepresentations;
  }

  public function getObjectSummary($id){
    $itemClient = $this->getItemClient('ca_objects', $id);

    $itemClient->setRequestBody(
      array("bundles" => array(
        "ca_objects.created.timestamp"  => array(
            "convertCodesToDisplayText" => true
        ),

        "ca_objects.preferred_labels.name" => array(),
        "ca_objects.idno" => array(),
        "ca_objects.description" => array(),
        "ca_objects.type_id" => array(
            "convertCodesToDisplayText" => true
        ),
        "ca_collections.related.idno" => array(),
        "ca_collections.related.type_id" => array("convertCodesToDisplayText" => true),
        "ca_collections.related.preferred_labels.name" => array("convertCodesToDisplayText" => true)
      ))
    );

    $result = $itemClient->request();
    $rawData = $result->getRawData();
    //print_r($rawData);
    $data = array();
    if(!array_key_exists('ca_collections.related.type_id', $rawData)){
      echo "huhoooo key does not exists";
      print_r($rawData);
    }
    $projectKey = array_search('project', $rawData['ca_collections.related.type_id']);

    $data['title']  = $rawData['ca_objects.preferred_labels.name'][0]['name'];
    if($projectKey !== false){
      $data['project'] = '<a href="'.$this->page->url().$rawData['ca_collections.related.idno'][$projectKey].'">'.
        $rawData['ca_collections.related.preferred_labels.name'][$projectKey]['name'].'</a>';
    }
    $data['type'] = $rawData['ca_objects.type_id'][0];
    if(!count($rawData['ca_objects.description']) == 0){
      if(key_exists('description', $rawData['ca_objects.description'][0]))
        $data['description'] = $this->strShorten($rawData['ca_objects.description'][0]['description'], 100);
    }
    $data['ref'] = $rawData['ca_objects.idno'][0];
    //$data['creation date'] = strftime('%B %Y', $rawData['ca_objects.created.timestamp'][0]);
    return $data;

    //print_r($result->getRawData());
    //return $result->getRawData();
  }

  public function getObjectsIdFromSeqId($id){
    $itemClient = $this->getItemClient('ca_collections', $id);
    $itemClient->setRequestBody(
      array(
        "bundles" => array(
          "ca_objects.idno" => array(

          )
        )
      )
    );
    $result = $itemClient->request();
    $rawData = $result->getRawData();
    $data = array();

    //print_r($rawData);
    if(!array_key_exists('ca_objects.idno', $rawData)){
      echo "no result!";
      print_r($rawData);

    }
    foreach($rawData['ca_objects.idno'] as $idno){
      $data[] = $idno;
    }
    return $data;
  }


  public function getPoleFromColId($id){
    $itemClient = $this->getItemClient('ca_collections', $id);
    $itemClient->setRequestBody(
      array(
        "bundles" => array(
          "ca_collections.pole"  => array(
            "convertCodesToDisplayText" => true,

          )
        )
      )
    );
    $result = $itemClient->request();
    $data = $result->getRawData();
    if(count($data['ca_collections.pole']) == 0){
      return false;
    }
    return $data['ca_collections.pole'][0]['pole'];
  }
  public function getProjectDetail($id){
    $itemClient = $this->getItemClient('ca_collections', $id);
    $itemClient->setRequestBody(
      array(
        "bundles" => array(
          "ca_collections.pole"  => array(
            "convertCodesToDisplayText" => true
          ),
          "ca_collections.preferred_labels.name" => array(),
          "ca_collections.idno" => array(),
          "ca_collections.related.idno" => array(),
          "ca_collections.related.preferred_labels.name" => array(),
          "ca_entities.related.preferred_labels.displayname" => array(),
          "ca_entities.related.idno" => array(),
          "ca_entities_x_collections.type_id" => array(
          )

        )
      )
    );
    $result = $itemClient->request();
    $rawData = $result->getRawData();
    $data = array();
    //print_r($rawData);
    $data['sequences'] = array();
    $data['entities'] = array();
    $data['title'] = $rawData['ca_collections.preferred_labels.name'][0]['name'];
    $data['ref'] = $rawData['ca_collections.idno'][0];
    if(array_key_exists('ca_collections.pole', $rawData) && count($rawData['ca_collections.pole']) > 0)
      $data['pole'] = $rawData['ca_collections.pole'][0]['pole'];
    $i = 0;
    //print_r($rawData["ca_entities.related.preferred_labels.displayname"]);
    foreach($rawData['ca_entities.related.idno'] as $entiId){
      $data['entities'][$entiId] = array(
        'name'=>$rawData["ca_entities.related.preferred_labels.displayname"][$i]['displayname'],
        'relation'=>$this->getRelationName($rawData["ca_entities_x_collections.type_id"][$i], 'ca_collections', 'ca_entities', 'project', true),
        'ref'=>$entiId
      );
      $i++;
    }
    $i = 0;
    foreach($rawData["ca_collections.related.idno"] as $seqId){
      $data['sequences'][$seqId] = array(
        'name'=> $rawData["ca_collections.related.preferred_labels.name"][$i]['name'],
        'objects' => $this->getObjectsIdFromSeqId($seqId),
        'ref' => $seqId
      );
      $i++;
    }
    //print_r($data);
    return $data;

  }

  public function getRelColFromColId($id){
    $itemClient = $this->getItemClient('ca_collections', $id);
    $itemClient->setRequestBody(
      array(
        "bundles" => array(
          "ca_collections.related.idno"  => array(
            "convertCodesToDisplayText" => true,
            "restrictToRelationshipTypes" => "part"
          ),
          "ca_collections.related.preferred_labels.name" => array()
        )
      )
    );
    $result = $itemClient->request();
    return $result->getRawData();
  }

  #region tools

  private function getItemClient($table, $id){
    $itemClient = new ItemService($this->CAurl, $this->CAuser, $this->CAkey, $table,"GET");
    $itemClient->addGetParameter('lang', $this->locale);

    $itemClient->addGetParameter('id', $id);
    return $itemClient;
  }

  private function getRelationName($relationId, $atable, $btable, $type = false, $reverse = false){

    switch($atable){
      case 'ca_objects':
        if($this->objectModel == null)
          $this->objectModel = $this->getObjectModel();
        $model = $this->objectModel;
        break;
      case 'ca_collections':

        if($this->collectionModel == null)
          $this->collectionModel = $this->getCollectionModel();
        $model = $this->collectionModel;
        break;
      case 'ca_entities':
      if($this->entityModel == null)
        $this->entityModel = $this->getEntityModel();
      $model = $this->entityModel;
      break;
    }
    //print_r($model);
    if($type !== false)
      $lookUpArr = $model[$type]['relationship_types'][$btable];
    else
      $lookUpArr = $model['relationship_types'][$btable];
    foreach($lookUpArr as $relation){
      if($relation['type_id'] == $relationId)
        return ($reverse)?$relation['typename_reverse']:$relation['typename'];
    }
    return false;
  }

  public function strShorten($str, $nbChars){
    //$str = utf8_encode($str);
    $str = strip_tags($str);
    $str = str_replace(array("\r", "\n"), '', $str);

    $newStr = mb_substr($str, 0, $nbChars);
    if(strlen($str) > strlen($newStr)){
      return $newStr.'…';
    }
    return $newStr;
  }

  private function switchLocale($array, $returnDefault = true){
    if(array_key_exists($this->locale, $array))
      return $array[$this->locale];
    else if($returnDefault){
      foreach($array as $content)
        return $content;
    }
    return [];
  }

  private function parseValues($result, $rawData, $fieldName){

    if(!array_key_exists('ca_objects.'.$fieldName, $rawData) ||
       count($rawData['ca_objects.'.$fieldName]) == 0)
      return $result;
    if(count($rawData['ca_objects.'.$fieldName]) == 1)
      return $this->parseSingleValue($result, $rawData, $fieldName);
    return $this->parseMultipleValues($result, $rawData, $fieldName);
  }
  private function parseSingleValue($result, $rawData, $fieldName){

    foreach($rawData['ca_objects.'.$fieldName] as $val){
      $val = $this->switchLocale($val);

      if(count($val) == 0 || !array_key_exists($fieldName, $val))
        return $result;

      $result[$fieldName] = $val[$fieldName];
      return $result;
    }
  }
  private function parseMultipleValues($result, $rawData, $fieldName){

    $values = array();
    foreach($rawData['ca_objects.'.$fieldName] as $val){
      //print_r($val);
      $val = $this->switchLocale($val, false);
      //print_r($val);
      if(count($val) == 0 || !array_key_exists($fieldName, $val))
        continue;
      $values[] = $val[$fieldName];
    }
    $result[$fieldName] = $values;
    return $result;
  }

  private function parseRelations($relations, $rawData, $tableName){
    $relations[$tableName] = array();
    if(array_key_exists($tableName, $rawData['related'])){
      $related = array();
      foreach($rawData['related'][$tableName] as $thing){
        $related[$thing['idno']] = array(
          'name' => (array_key_exists('name', $thing))?$thing['name']:$thing['displayname'],
          'relation' => $thing['relationship_typename']
        );
      }
      $relations[$tableName] = $related;
    }
    return $relations;

  }

  #endregion tools


}

?>
