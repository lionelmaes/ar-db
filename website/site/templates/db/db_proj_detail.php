<?php

$currentProject = $ca->getProjectDetail($input->urlSegment1);
$currentSequence = false;

if($input->urlSegment2 != '' && array_key_exists($input->urlSegment2, $currentProject['sequences'])){
  $currentSequence = $currentProject['sequences'][$input->urlSegment2];
}else{
  foreach($currentProject['sequences'] as $idno => $sequence){
    $currentSequence = $sequence;
    break;
  }
}

?>
<section class="project">
  <header>
    <h3 class="h-scroll">
      <?php if(array_key_exists('pole', $currentProject)):?>
      <span class="pole"><?php echo $currentProject['pole']; ?></span>
      <?php endif; ?>
      <span class="project"><?php echo $currentProject['title']; ?></span>
    </h3>
<?php
if(count($currentProject['entities'])>-1):

?><section class="relations relations-entities"><ul class="h-scroll"><?php
  foreach($currentProject['entities'] as $entity):
    //print_r($entity);
?><li><span class="relation-type"><?php echo $entity['relation']; ?></span> <a href="<?php echo $page->url.$entity['ref'];?>"><?php echo $entity['name']; ?></a></li>
<?php
  endforeach;
?></ul></section><?php
endif;
?>

  </header>
  <ul class="sequence-menu h-scroll">
<?php
foreach($currentProject['sequences'] as $idno => $sequence):
?>
    <li<?php echo ($idno == $currentSequence['ref'])?' class="active"':''; ?>>
      <a href="<?php echo $page->url().$input->urlSegment1.'/'.$idno; ?>">
        <?php echo $sequence['name']; ?>
      </a>
  </li>
<?php
endforeach;
?>
  </ul>
  <div class="timeline">
  </div>
  <section class="sequence">

<?php
if($currentSequence != false):
  //print_r($currentSequence);
  $objCounter = 1;
  foreach($currentSequence['objects'] as $docId):

    include('db_doc_detail.php');
?>

<?php
    $objCounter++;
  endforeach;
endif;//sequence exists
?>
</section>
</section>
