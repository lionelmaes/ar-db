<?php
/*listing of documents*/
?>
<?php 
if(!$config->ajax || isset($parameters['action']) && $parameters['action'] == 'reload'): 
  //we just make the structure of the container
?>
<section id="search-results">
  <div class="document-summary loader">
  </div>
</section>

<?php
else://this is an ajax call. time to put some content inside search-results
  $limit = 10;
  $start = (isset($parameters['listStart']))?$parameters['listStart']:0;
  
  $items = $ca->getListing($facets, $start, $limit);
  //print_r($items);
  //$items['results'] = array_reverse($items['results']);
  foreach($items['results'] as $item):
    $summary = $ca->getObjectSummary($item['id']);

?>
<div class="document-summary">
  <a class="post-link" href="<?php echo $page->url;?><?php echo $summary['ref']; ?>"></a>
  <ul>
<?php
    foreach($summary as $label=>$value):
?>
      <li><span class="label"><?php echo __($label, $tr); ?>:</span> <span class="value"><?php echo $value; ?></span></li>
<?php
    endforeach;
?>
  </ul>

</div>
<?php
  endforeach;
endif;
?>
  
