async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.text(); // parses JSON response into native JavaScript objects
}
function unify(e) { return e.changedTouches ? e.changedTouches[0] : e };
function go_hash(hash) {
  console.log('go_hash: ' + hash)
  if(hash.indexOf('#') == -1)
    hash = '#' + hash
  if(document.location.hash) {
    document.location.hash = hash
    return
  }
  if(window.location.hash) {
    window.location.hash = hash
    return
  }
  if(document.location.href) {
    document.location.href = hash
    return
  }
  window.location.href = hash
}
function isScrolledIntoView(el, headerHeight = 0) {
    const rect = el.getBoundingClientRect();
    const elemTop = rect.top;
    const elemBottom = rect.bottom;
    
    const visibleHeight = window.innerHeight - headerHeight;
    //console.log('elemTop'+elemTop);
    //console.log('elemBottom'+elemBottom);
    // Only completely visible elements return true:
    //var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // Partially visible elements return true:
    //var isVisible = elemTop < window.innerHeight && elemBottom >= window.innerHeight;


    /*
    console.log('elemTop : '+elemTop);
    console.log('elemBottom : '+ elemBottom);
    console.log('inner height: '+visibleHeight);
    */
   //console.log(elemTop);
   //console.log(visibleHeight);
    var isVisible = (elemTop < window.innerHeight && elemBottom > window.innerHeight)
                    || (elemTop >= headerHeight && elemTop < window.innerHeight) 
                    || (elemBottom < window.innerHeight && elemBottom >= headerHeight);
    return isVisible;
}

function scrollIntoView(pageElement, headerHeight = 0){
  
    var positionX = 0,         
        positionY = headerHeight;    

    while(pageElement != null){        
        positionX += pageElement.offsetLeft;        
        positionY += pageElement.offsetTop;        
        pageElement = pageElement.offsetParent;        
        window.scrollTo(positionX, positionY);    
    }

}

var colc;

function masonry(){
    // selector string as first argument
    /*console.log('ok lets do the masonry');
  colc = new Colcade( '#search-results', {
    columns: '.grid-col',
    items: '.document-summary'
  });*/
}

/*transitions*/
// Show an element
function show(elem) {

  // Get the natural height of the element
  var getHeight = function() {
    if(elem.style.display == 'none')
      elem.style.display = 'block'; // Make it visible
    var height = elem.scrollHeight + 'px'; // Get it's height
    elem.style.display = ''; //  Hide it again
    return height;
  };

  var height = getHeight(); // Get the natural height
  elem.classList.add('visible'); // Make the element visible
  elem.style.height = height; // Update the max-height

  // Once the transition is complete, remove the inline max-height so the content can scale responsively
  window.setTimeout(function() {
    elem.style.height = '';
  }, 350);

};

// Hide an element
function hide(elem) {
  elem.setAttribute('data-display', elem.style.display);
  // Give the element a height to change from
  elem.style.height = elem.scrollHeight + 'px';

  // Set the height back to 0
  window.setTimeout(function() {
    elem.style.height = '0';
  }, 30);

  // When the transition is complete, hide it
  window.setTimeout(function() {
    elem.classList.remove('visible');
    elem.style.height = '';
  }, 350);

};

// Toggle element visibility
var toggle = function(elem, timing) {

  // If the element is visible, hide it
  if (elem.classList.contains('visible')) {
    hide(elem);
    return;
  }

  // Otherwise, show it
  show(elem);

};

function slider(event) {
  let targetSelector = event.target.getAttribute('data-target');
  if (targetSelector == null) return;
  let targetElement = document.querySelector(targetSelector);
  if (targetElement.classList.contains('visible')) {
    hide(targetElement);
    event.target.classList.remove('close');
  } else {
    show(targetElement);
    event.target.classList.add('close');
  }
}

function initSliders() {
  var sliderButtons = document.querySelectorAll('.slider-button');
  for (let i = 0; i < sliderButtons.length; i++) {
    sliderButtons[i].removeEventListener('click', slider);
    sliderButtons[i].addEventListener('click', slider);
  }
}



function showContent(els) {
  const baseURL = document.querySelector('main').getAttribute('data-base-url');
  
  els.forEach(el => {
    var title = el.firstElementChild;
    var id = el.id;
    title.addEventListener('click', e => {
      var classes = el.className.split(' ');
      if (classes.includes('visible') == true) {
        els.forEach(el => {
            el.classList.remove('visible');
          });
          history.pushState(null, null, baseURL);
          //window.location.hash = '';
      } else {
        els.forEach(el => {
            el.classList.remove('visible');
          })
        el.classList.add('visible');
        //scrollIntoView(el);
        el.scrollIntoView(true);
        window.history.pushState({'id':id}, null, baseURL+id);
        
      }
    })
  });

  window.addEventListener('popstate', (e) => {
    els.forEach(el => {
      el.classList.remove('visible');
    });
    if(e.state != null){
     
      const el = document.getElementById(e.state.id);
      el.classList.add('visible');
      el.scrollIntoView(true);
    }
  });
}




function SlideShow(els) {
  els[0].classList.add('show')
  var j = 0
  for (let i = 0; i < els.length; i++) {
    const el = els[i];
    el.addEventListener('click', function() {
      el.classList.remove('show');
      var nextEl = (el.nextElementSibling == null) ? els[0] : el.nextElementSibling;
      if (nextEl == null) {

      }
      nextEl.classList.add('show');
    })

  }

}
function subScroll(){
  console.log('SUBSCROLL');
  const subScrollMenu = document.querySelector('.sub-scroll');
  const links = subScrollMenu.querySelectorAll('.sub-scroll li');
  const frames = document.querySelectorAll('main section');
  const headerHeight = document.querySelector('.sequence-menu').getBoundingClientRect().bottom;
  console.log(headerHeight);
  let currentStep = 0;
  let oldStep = 0;

  const spyAndUpdate = () => {
    
    if(document.documentElement.scrollTop == 0){
      currentStep = 0;
    }else{
      
      for(let i = 0; i < frames.length; i++){
        
        if(isScrolledIntoView(frames[i], headerHeight)){
          
          currentStep = i;
          break;
        }
      }
    }
    if(oldStep != currentStep){
      
      update();
      oldStep = currentStep;
      
    }

  };

  const update = () => {
    links[oldStep].classList.remove('active');
    links[currentStep].classList.add('active');
    //links[currentStep].scrollIntoView(true);
  };
  
  window.addEventListener('scroll', spyAndUpdate);

  

}

function timeline(){

  //var init
  const container = document.querySelector('.timeline');
  const attachedElement = document.querySelector('ul.sequence-menu');
  const frames = document.querySelectorAll('.object-detail');
  const sequence = document.querySelector('.sequence');
  const headerHeight = document.querySelector('.sequence-menu').getBoundingClientRect().bottom;
  const stepsNb = frames.length;
  let activeSpy = true;
  let wH;
  let currentStep = -1;
  let oldStep = -1;
  const stepElts = [];

  for(var i = 0; i < stepsNb; i++){
    let stepElt = document.createElement('A');
    stepElt.href='#obj-'+(i+1);
    stepElt.classList.add('step');
    stepElt.innerText = i+1;
    container.appendChild(stepElt);
    stepElts.push(stepElt);
  }
  let factor = 1;
  if(stepsNb > 1){
    factor = (stepsNb < 4)?stepsNb-1:3;
  }


  //dimensions related const
  let containerDim, ch, normalStepHeight, currentStepHeight, totalAvailableH;


  const updateDimensions = () => {
    attachedElementDim = attachedElement.getBoundingClientRect();
    containerDim = container.getBoundingClientRect();
    if(containerDim.top != attachedElementDim.bottom
       || ch == undefined
       || wH != window.innerHeight
     ){
      wH = window.innerHeight;
      container.style.top = attachedElementDim.bottom+'px';

      container.style.height = wH - 8 - attachedElementDim.bottom+'px';
    }else{
      return false;
    }

    ch = wH - 8 - attachedElementDim.bottom;

    normalStepHeight = ch / stepsNb;

    currentStepHeight  = (normalStepHeight * factor < 20)?20:normalStepHeight * factor;
    totalAvailableH = ch - currentStepHeight;
    return true;
  };



  const spyAndLaunch = () => {
    if(!activeSpy)return;
    let dimensionUpdate = updateDimensions();
    if(document.documentElement.scrollTop == 0){
      currentStep = 0;
    }else{
      for(let i = 0; i < frames.length; i++){
        if(isScrolledIntoView(frames[i], headerHeight)){
          currentStep = i;
          break;
        }
      }
    }
    if(oldStep != currentStep){
      //console.log(currentStep);
      updateTimeline();
      oldStep = currentStep;
    }else if(dimensionUpdate){

      updateTimeline();
    }


  };

  const updateTimeline = () => {

    let availableH, nbStepsBeforeCurrent, factor, stepHeight;
    //normalStepHeight
    //currentStepHeight
    //totalAvailableH

    //first we resize the currentStep element
    stepElts[currentStep].style.height = currentStepHeight+'px';
    stepElts[currentStep].classList.add('current');

    if(currentStep > 0){
      //we have to distribute the totalAvailableH before and after the currentStep Element
      //availableH = totalAvailableH / 2

      nbStepsBeforeCurrent = currentStep;
      availableH = totalAvailableH / (stepsNb - 1) * nbStepsBeforeCurrent;
      //availableH = (stepsNb > currentStep + 1) ? totalAvailableH / 2 : totalAvailableH;
      for(let i = currentStep - 1; i >= 0; i--){
        //we divide the availableH by the number of steps to make before currentStep. If this is the last one, the factor should be 1
        factor = (nbStepsBeforeCurrent == 1)?1:1.5;
        stepHeight = (availableH / nbStepsBeforeCurrent) * factor;
        stepElts[i].style.height = stepHeight+'px';
        stepElts[i].classList.remove('current');
        availableH -= stepHeight;

        nbStepsBeforeCurrent--;

      }

    }

    nbStepsBeforeCurrent = stepsNb - 1 - currentStep;
    availableH = totalAvailableH / (stepsNb - 1) * nbStepsBeforeCurrent;
    //availableH = (currentStep > 0)? totalAvailableH / 2 : totalAvailableH;
    for(let i = currentStep + 1; i < stepsNb; i++){

      factor = (nbStepsBeforeCurrent == 1)?1:1.5;
      //factor = 1;
      stepHeight = (availableH / nbStepsBeforeCurrent) * factor;

      stepElts[i].style.height = stepHeight+'px';
      stepElts[i].classList.remove('current');

      availableH -= stepHeight;

      nbStepsBeforeCurrent--;

    }


  };

  const scrollToFrame = (index) => {
    let elt = frames[index];
    console.log(elt.getBoundingClientRect());
    console.log(sequence.offsetTop);
    window.scrollBy(0, elt.getBoundingClientRect().y - sequence.offsetTop + 9);
  };

  //events and direct calls
  updateDimensions();

  window.addEventListener('scroll', spyAndLaunch, false);
  window.addEventListener('resize', spyAndLaunch);

  stepElts.forEach((elt, i) => {
    /*
    elt.addEventListener('click', (e) => {
      scrollToFrame(i);
    });
    */
    elt.addEventListener('mouseenter', (e) => {
      e.stopPropagation();
      currentStep = oldStep = i;
      activeSpy = false;
      //console.log(currentStep);

      updateTimeline();
    });
    elt.addEventListener('mouseleave', (e) => {
      e.stopPropagation();
      activeSpy = true;
      spyAndLaunch();
    });
    sequence.addEventListener('mouseenter', (e) => {
      activeSpy = true;
    });
    sequence.addEventListener('touchstart', (e) => {
      activeSpy = true;
    });

/*
    elt.addEventListener('touchstart', (e) => {

      touchEvt = true;
      if(currentStep != i){
        currentStep = oldStep = i;
        updateTimeline();
        activeSpy = false;
        window.setTimeout((e) => {activeSpy = true;}, 1000);
        go_hash('obj-'+(i+1));
      }

    });
    */

      //alert(i);
  });


  spyAndLaunch();






}

function initHScrolls(){

  let elts = document.querySelectorAll('.h-scroll');
  let goRightElt = document.createElement('DIV');
  goRightElt.classList.add('go-right');
  let goLeftElt = document.createElement('DIV');
  goLeftElt.classList.add('go-left');

  elts.forEach(el => {
    let gRE = goRightElt.cloneNode(true);
    let gLE = goLeftElt.cloneNode(true);

    el.appendChild(gRE);
    el.prepend(gLE);
    el.addEventListener('scroll', () => {
      updateHScroll(el, gRE, gLE);
    });

  });

  const updateHScroll = (el, gRE, gLE) => {
    if(el.scrollLeft+el.clientWidth < el.scrollWidth){
      gRE.classList.add('visible');

    }else{
      gRE.classList.remove('visible');
    }
    if(el.scrollLeft > 0){
      gLE.classList.add('visible');
    }
    else{
      gLE.classList.remove('visible');
    }

  };
  const updateHScrolls = () => {
    elts.forEach(el => {
      let gRE = el.querySelector('.go-right');
      let gLE = el.querySelector('.go-left');
      updateHScroll(el, gRE, gLE);
      
    });
  };

  window.addEventListener('resize', updateHScrolls);
  updateHScrolls();


}

async function newsLoader(){
  let loadBars = document.querySelectorAll('div.loading');
  const headerHeight = document.querySelector('nav').getBoundingClientRect().bottom;
  const newsContainer = document.querySelector('main.news-tpl');

  
  const update = async () => {
    loadBars = document.querySelectorAll('div.loading');
    for(let i = 0; i < loadBars.length; i++){
      let el = loadBars[i];
      //console.log(el);
      //console.log(isScrolledIntoView(el, headerHeight));
      
      if(isScrolledIntoView(el, headerHeight) ){
        //console.log('loading')
        el.parentNode.removeChild(el);
        await loadNews(el.getAttribute('data-direction'), el.getAttribute('data-section'));
        
      }
    }
   
  };

  const loadNews = async (direction, section) => {
    //console.log('load');
    postData(document.URL, {'direction': direction, 'section': section}).then(html => {
      
      let frag = document.createDocumentFragment();
      let wr = document.createElement('div');
      wr.innerHTML = html;
      let loadBar = wr.querySelector('div.loading[data-direction="'+direction+'"]');
      
      let newsElts = wr.querySelectorAll('section');

      for(let i = 0; i < newsElts.length; i++){
        frag.appendChild(newsElts[i]);
      }

      
      if(direction == 'fwd'){
        if(loadBar != null)
          frag.appendChild(loadBar);
        newsContainer.append(frag);
      }else{
        if(loadBar != null)
          frag.prepend(loadBar);
        newsContainer.prepend(frag);
      }
      
      showContent(newsElts);

      

      
      update();
      //console.log(html);
    });
    
  }

  



  /*

  loadBars.forEach(el => {
    el.style.display = 'block';
    if(el.getAttribute('data-direction') == 'back'){
      window.scrollBy({'top':el.offsetHeight, behavior: 'instant' });
    }
  });

  */

  window.addEventListener('scroll', update); 

  update();

}


function init(){
  initSliders();
  initHScrolls();
  console.log('init');
  if(document.querySelector('.news-tpl') != null){
    const targetSection = document.querySelector('.news-tpl section.visible');
    if(targetSection != null){
      let backLoad = document.querySelector('.loading[data-direction="back"]');
      if(backLoad != null){
        //console.log('adjust');
        window.scrollBy({'top':backLoad.offsetHeight, behavior: 'instant' });
      }
      //targetSection.scrollIntoView(true, {behavior: 'instant' });

    }

    var news = document.querySelectorAll('.news-tpl section')
    showContent(news);
    newsLoader();
    
  }
  if(document.querySelector('.document-media') != null){
    var medias = document.querySelectorAll('.document-media')
    SlideShow(medias)
  }
    if(document.querySelector('section.project') != null){
      //dbStickyMenu();
    }
  if(document.querySelector('#search-results') != null){
    masonry();
  }
  if(document.querySelector('.timeline') != null){
    timeline();
  }
  if(document.querySelector('.sub-scroll') != null){
    subScroll();
  }

}
init();
