function initAutoComplete(data){
  const autoCompletejs = new autoComplete({
    data: {
      src: data,
      key: ["title"],
      cache: true
    },
    trigger: {
      event: ["input", "focusin", "focusout"]
    },
    placeHolder: searchDefaultMessage,
    searchEngine: "strict",
    highlight: true,
    maxResults: 5,
    resultsList: {
      render: true
    },
    sort: (a, b) => {                    // Sort rendered results ascendingly | (Optional)
        if (a.match < b.match) return -1;
        if (a.match > b.match) return 1;
        return 0;
    },
    resultItem: {
          content: (data, source) => {
              //console.log(data);
              source.innerHTML = data.value.facet+' '+data.match;
          }
    },
    noResults: function() {
      return;
    },
    onSelection: function(feedback) {
      document.querySelector("#autoComplete").blur();
      const selection = feedback.selection.value.title;
      addFacet(feedback.selection.value.facetkey,
               feedback.selection.value.id,
               feedback.selection.value.facet,
               feedback.selection.value.title
             );
      //console.log(feedback.selection.value);
      // Render selected choice to selection div
      //document.querySelector(".selection").innerHTML = selection;
      // Clear Input
    //  document.querySelector("#autoComplete").value = "";
      // Change placeholder with the selected value
      //document.querySelector("#autoComplete").setAttribute("placeholder", selection);
      /// Concole log autoComplete data feedback
      //console.log(feedback);
    },
  });

  // Toggle event for search input
  // showing & hidding results list onfocus / blur
  ["focus", "blur"].forEach(function(eventType) {
    const resultsList = document.querySelector("#autoComplete_list");

    document.querySelector("#autoComplete").addEventListener(eventType, function() {
      // Hide results list & show other elemennts
      if (eventType === "blur") {

        resultsList.style.display = "none";
      } else if (eventType === "focus") {
        // Show results list & hide other elemennts

        resultsList.style.display = "block";
      }
    });
  });
}

function removeFacet(facetKey, facetValue){
  console.log(facetKey);
  let currentFacets = facets[facetKey];
  for(let i = 0; i < currentFacets.length; i++){
    let facet = currentFacets[i];
    if(facet.value == facetValue){
      currentFacets.splice(i, 1);
      break;
    }
  }
  facets[facetKey] = currentFacets;
  if(facets[facetKey].length == 0){
    delete facets[facetKey];
  }

  updateListing();
}

function addFacet(facetKey, facetValue, facetLabel, facetText){
  let facet = {
    'value':facetValue,
    'label':facetLabel,
    'text':facetText
  };
  if(facets[facetKey] == undefined){
    facets[facetKey] = [];
  }
  facets[facetKey].push(facet);
  updateListing();
}
let startListing = 0;
function updateListing(){
  searchUIElem.parentElement.removeChild(searchUIElem);
  searchResultsElem.parentElement.removeChild(searchResultsElem);
  startListing = 0;
  postData(document.URL, {'facets':facets, 'action':'reload'})
  .then(html => {
    mainElem.innerHTML = html;
    searchUIElem = document.getElementById('search-ui');
    searchResultsElem = document.getElementById('search-results');
    initSliders();
    initEvents();
    //console.log(data.text()); // JSON data parsed by `response.json()` call
  });


}

function addItemsToListing(){
  window.removeEventListener('scroll', listElemSpyAndUpdate);
  postData(document.URL, {'facets':facets, 'listStart':startListing})
  .then(html => {
    let loaderDiv = document.querySelector('div.document-summary.loader');
    loaderDiv.parentNode.removeChild(loaderDiv);
    if(html != ''){
      
      searchResultsElem.innerHTML = searchResultsElem.innerHTML + html;
      searchResultsElem.appendChild(loaderDiv);
      startListing += 10;
      const headerHeight = document.querySelector('nav').getBoundingClientRect().bottom;
      if(isScrolledIntoView(loaderDiv, headerHeight)){
        addItemsToListing();
      }
      else
        window.addEventListener('scroll', listElemSpyAndUpdate);
    }
  });
}
function listElemSpyAndUpdate(){
  
  let loaderDiv = document.querySelector('div.document-summary.loader');
  if(loaderDiv != null){
    
    const headerHeight = document.querySelector('nav').getBoundingClientRect().bottom;
    if(isScrolledIntoView(loaderDiv, headerHeight)){
      addItemsToListing();
    }
  }
}

function initEvents(){
  if(document.querySelector('#search-filters') == null)
    return;
  const searchObjects = [];
  const facetElems = document.querySelectorAll('#search-filters span.facet');
  for(let i = 0; i < facetElems.length; i++){
    //console.log(facetElems[i]);
    searchObjects.push({
      facetkey:facetElems[i].getAttribute('data-facet-key'),
      facet:facetElems[i].getAttribute('data-facet-label'),
      title:facetElems[i].innerText,
      id:facetElems[i].getAttribute('data-facet-value')
    });

    facetElems[i].addEventListener('click', function(e){
      addFacet(this.getAttribute('data-facet-key'),
               this.getAttribute('data-facet-value'),
               this.getAttribute('data-facet-label'),
               this.innerText
             );
    });
  }
  const facetParamsElems = document.querySelectorAll('.facet-param');

  for(let i = 0; i < facetParamsElems.length; i++){
    facetParamsElems[i].addEventListener('click', function(e){

      removeFacet(this.getAttribute('data-facet-key'), this.getAttribute('data-facet-value'));
    });
  }
  initAutoComplete(searchObjects);

  console.log(searchResultsElem);
  if(searchResultsElem != null){
    addItemsToListing();
    window.addEventListener('scroll', listElemSpyAndUpdate);
  }


}

const facets = {};

var searchUIElem = document.getElementById('search-ui');
var searchResultsElem = document.getElementById('search-results');
const mainElem = document.querySelector('main');

initEvents();
