<?php
/**
 * TRANSLATABLE STRINGS
 * Include translatable strings
 * @see /site/translations.php
 * @var $ferry
 */

$this->wire('tr', '/site/translate.php', true);
?>
